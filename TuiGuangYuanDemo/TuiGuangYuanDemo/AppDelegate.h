//
//  AppDelegate.h
//  TuiGuangYuanDemo
//
//  Created by DingXiao on 14-6-16.
//  Copyright (c) 2014年 DingXiao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
