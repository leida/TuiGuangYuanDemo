//
//  TuiGuangYuan.h
//  TuiGuangYuan
//
//  Created by DingXiao on 14-6-15.
//  Copyright (c) 2014年 DingXiao. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TuiGuangYuan_APP_KEY @"10003"    //appKey
#define TuiGuangYuan_APP_SECRECT @"a114abc6994c67932egk" //appSecret


@interface TuiGuangYuan : NSObject
/**
 *   active the app With the appKey and appSecrect;
 */
+ (void)activeTuiGuangYuanWithAppKey:(NSString *)appKey andAppScrect:(NSString *)appSecret;

/**
 *   show the promotion vc
 */
+ (void)showTuiGuangYuanPromotionWith:(UIViewController *)viewController andAppkey:(NSString *)appKey andAppSecret:(NSString*)appSecret;

@end
