//
//  TuiGuangYuanActive.h
//  TuiGuangYuanActive
//
//  Created by DingXiao on 14-6-15.
//  Copyright (c) 2014年 DingXiao. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TuiGuangYuanActive : NSObject

/**
 *   active the app With the appKey and appSecrect;
 */
+ (void)activeTuiGuangYuanWithAppKey:(NSString *)appKey andAppScrect:(NSString *)appSecret;

@end
