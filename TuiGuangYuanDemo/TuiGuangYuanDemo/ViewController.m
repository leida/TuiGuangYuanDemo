//
//  ViewController.m
//  TuiGuangYuanDemo
//
//  Created by DingXiao on 14-6-16.
//  Copyright (c) 2014年 DingXiao. All rights reserved.
//

#import "ViewController.h"
#import "TuiGuangYuan.h"
#import "TuiGuangYuanActive.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

- (IBAction)showThePromotionVCAction:(id)sender;
@end

@implementation ViewController

- (void)showThePromotionVCAction:(id)sender
{
    NSLog(@"___");
    
    [TuiGuangYuan activeTuiGuangYuanWithAppKey:TuiGuangYuan_APP_KEY andAppScrect:TuiGuangYuan_APP_SECRECT];
    [TuiGuangYuan showTuiGuangYuanPromotionWith:self andAppkey:TuiGuangYuan_APP_KEY andAppSecret:TuiGuangYuan_APP_SECRECT];
//    [TuiGuangYuanActive activeTuiGuangYuanWithAppKey:TuiGuangYuan_APP_KEY andAppScrect:TuiGuangYuan_APP_SECRECT];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableArray *colorArray = [@[[UIColor orangeColor],[UIColor yellowColor]] mutableCopy];
    UIImage *backImage = [self buttonImageFromColors:colorArray];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:backImage]];
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage*) buttonImageFromColors:(NSArray*)colors{
    NSMutableArray *ar = [NSMutableArray array];
    for(UIColor *c in colors) {
        [ar addObject:(id)c.CGColor];
    }
    UIGraphicsBeginImageContextWithOptions(self.view.frame.size, YES, 1);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGColorSpaceRef colorSpace = CGColorGetColorSpace([[colors lastObject] CGColor]);
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)ar, NULL);
    CGPoint start;
    CGPoint end;
    start = CGPointMake(0.0, 0.0);
    end = CGPointMake(0.0, self.view.frame.size.height);
    CGContextDrawLinearGradient(context, gradient, start, end, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGContextRestoreGState(context);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
    return image;
}


@end
