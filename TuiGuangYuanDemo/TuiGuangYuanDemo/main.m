//
//  main.m
//  TuiGuangYuanDemo
//
//  Created by DingXiao on 14-6-16.
//  Copyright (c) 2014年 DingXiao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
